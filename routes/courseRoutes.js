const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseControllers.js")


router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})



/*
Create a route that will let an admin performa addCourse function in the courseController
	verify that the user is logged in
	decode the token for that user
	use the id, isAdmin, and request body to perform the function in courseController
		id and isAdmin are parts of an object inside the token
*/

/*
	Create a route that will revtrieve all of our products/courses
		will require login/register functions
*/

router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	courseController.getActive().then(resultFromController => res.send(resultFromController))
})

/*
miniactivty
	create a route that will retrieve a course hint: params
		will not require login/register from the users
*/
// retrieve a course
router.get("/:courseId",  (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params.courseId).then(result => res.send(result))
})

// update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})
/*
delete is never a norm in databases

use "/:courseId/archiveCourse" and send a PUT request to archive a course by changing the active status
*/

// archiving a course
router.put("/:courseId/archive", auth.verify, (req,res)=>{
	courseController.archivedCourse(req.params, req.body).then(result =>res.send(result))
	/*
		if you do not want to require any request body
		courseController.archivedCourse(req.params).then(result =>res.send(result))
	*/
})

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		courseId: auth.decode(req.headers.authorization).id,
		userId: req.body.userId
	}
	courseController.updateCourse(data).then(result => res.send(result))
})

module.exports = router;