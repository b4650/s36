// set up dependencies

const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
// used to encrypt user passwords
const bcrypt = require("bcrypt")

// check if the email exists
/*
	1. Check for the email in the database
	2. Send the result as a response (with error handling)
*/
/*
	it is conventional for the devs to use boolean in sending return responses especially with the backend application
*/


module.exports.checkEmail = (requestBody => {
	return User.find({email: requestBody.email}).then((result,error) => {
		if (error) {
			console.log(error)
			return false
		}else {
			if (result.length > 0) {
				// return result
				return true
			}else {
				// return res.send("email does not exist")
				return false
			}
		}
	})
})

// USER REGISTRATION
/*
	1. Create a new user with information from the requestBody
	2. Make sure that the password is encrypted
	3. Save the new user
*/

module.exports.registerUser = (requestBody) => {
	let newUser = new User ({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		age: requestBody.age,
		gender: requestBody.gender,
		email: requestBody.email,
		// performing algorithms // hashSync is a function of bcrypt that encrypts the password
		// 10 the number of rounds/times it runs the algorithm to the reqBody.password
			// maximum is 72
		password: bcrypt.hashSync(requestBody.password, 10),
		mobileNo: requestBody.mobileNo
	})
	return newUser.save().then((saved, error) => {
		if(error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// USER LOGIN
/*
	1. find if the email is existing in the database
	2. Check if the password is correct
*/

module.exports.userLogin = (requestBody) => {
	return User.findOne( {email: requestBody.email} ).then(result => {
		if (result === null) {
			return false
		}else {
			// compareSync is used to compare a non=encrypted password to an encrypoted password and returns a boolean responsedepending on the result
			/*
				true - a token s hould be created since the user is existing and the password is correct
				false - the passwords do not match, thus a token should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if (isPasswordCorrect){
				// auth - imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transform the User into an Object that can be used by our createAccessToken 
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}

/*module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result =>{
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}*/

module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result =>{
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollments.push( {courseId: data.courseId} )

		// saving in the database
		return user.save().then((user, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})
	
	/*
		use another await keyword to update the enrollees array in the course collection

			find the courseId from the requestBody
			push the userId of the enrollee in the enrollees array of the course
			update the document in the database



			if both pushing are successful, return true
			if both pushing are not successful, return false
	*/

	if (isUserUpdated ){
		return true
	}
	else{
		return false
	}
}


/*
Deploying a server

1. register in Heroku (https://signup.heroku.com/login)
2. check if you have heroku in your device
3. create a "Procfile" file
		- it should have P
		- it should not have any file extensions
		- inserting the text "web: node app"
4. login your heroku through the git bash/terminal
		- "heroku login"
				press any key
				log in using your heroku account
				there should be confirmation
5. heroku create to create/deploy your server app to a host in the internet
6. git push heroku master to push the updates in heroku
*/