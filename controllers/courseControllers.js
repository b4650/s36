const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")


/*
	import the necessary models that you will need to perform CRUD operations

	1. Find the user in the database 
		Find out if the user is admin

	2. if the user is not an admin, send the response "You are not an admin"

	3. if the user is an admin, create the new COurse object
		save the object
			if there are errors, return false
			if the are no errors , return "Course created successfully"

*/

module.exports.addCourse = (requestBody, userData) => {
	return User.findOne({ email: userData.email, isAdmin: userData.isAdmin}).then(result =>{
		if(userData.isAdmin === false){
			return "You are not an admin"
		}else {
			let newCourse = new Course ({
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price
			})
			return newCourse.save().then((saved, error) => {
				if(error){
					console.log(error)
					return false
				}else{
					return true
				}
			})
		}
	})
}


// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

/*
	using the course id (params) in the url, retrieve a course using a get request
		send the code snippet in the batch google chat
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	})
}

// retrieve all active courses
module.exports.getActive = () => {
	return Course.find( { isActive: true } ).then((result,error) => {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}

// update a course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

// archive a course
/*
	1. create an updateCourse object with the content from the reqBody
			reqBody should have the isActive status of the course to be set to false
	2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
			handle the errors that may arise
				error/s - false
				no errors - true
*/

// archiving a course
module.exports.archivedCourse=(reqParams, reqBody)=>{
	let updatedCourse = {
		isActive: reqBody.isActive
	} 

/*
if you do not want to require any request body:
module.exports.archivedCourse=(reqParams)=>{
	let updatedCourse = {
		isActive: false
	} 
*/

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error)=>{
			if (error) {
				return false
			}else{
				return true
			}
	})
}

module.exports.updateCourse = async (data) => {
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push( {userId: [data.userId]} )
		return course.save().then((course, err) =>{
			if (err){
				return false
			}else {
				return true
			}
		})
	})
	if (isCourseUpdated){
		return true
	}else {
		return false
	}
}