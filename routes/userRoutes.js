const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userControllers.js")

/*
	create a function with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController
	reminder that we have a parameter needed in the checkEmail function and that is data from request body.
*/

// checking of email
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})


//user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

router.post("/login", (req,res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})

/*//auth.verify - ensures that the user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res) => {
	// decode - decrypts the token inside the authorization (which is in the headers of the request)
	// req.headers.authorization - contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	console.log(userData.id)
	userController.getProfile({userId: userData.id}).then (result => res.send(result)
		)
})*/

// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res)=> {
	// decode - decrypts the token inside the authorization (which is in the headers of the request) 
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result))
})


module.exports = router;